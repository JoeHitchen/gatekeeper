#!/bin/bash
target=$1
main_host=$2
extra_hosts=""
for ((i=3;i<=$#;i++)); do
	extra_hosts="$extra_hosts ${!i}"
done

config_file="config/$main_host.site.conf"
cp config/site.conf.template $config_file

sed -i "s=<proxy_target\:http\://localhost\:8080>=$target=g" $config_file
sed -i "s=<domain\:example.com>=$main_host=g" $config_file
sed -i "s= \[<extra_domain\:www.example.com>\]=$extra_hosts=g" $config_file

admin_email=`cat settings/admin_email.txt`
cert_cmd="docker-compose run --rm certbot certonly --webroot -w /var/www/challenge -n --email $admin_email --no-eff-email --agree-tos"
for ((i=2;i<=$#;i++)); do
	cert_cmd="$cert_cmd -d ${!i}"
done
$cert_cmd

docker-compose up --force-recreate -d nginx
