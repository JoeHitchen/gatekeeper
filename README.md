# Gatekeeper

_This project is now deprecated in favour of [Traefik](https://traefik.io/traefik/)._

Gatekeeper is a simple Nginx reverse-proxy and SSL termination server, with certificate management provided by Certbot.

* Easily configurable reverse-proxy server.
* Redirect all requests to https.
* Simple certificate generation for new domains.
* Automatic certificate renewal/redeployment.

## Setting up Gatekeeper
Gatekeeper provides a setup script, `setup.sh`, to automate initial setup.
It creates a 2048 bit Diffie Hellman key for SSL security, and then loads the NGINX service.
To verify the container loaded, run `docker ps -a` and look for a container named `gatekeeper_nginx_1`.
__The script must be called from the Gatekeeper root.__

To simplify adding sites, the setup script can also provided with an admin e-mail address (e.g. `setup.sh admin@domain.com`).
This is then stored in the `settings/admin_email.txt` file for later use when creating certificates.

_See also Automatic Renewals below._

## Adding a new site
Gatekeeper provides a script for quickly setting up new sites, which is called as follows:  
`./add_site.sh <target> <main_domain> [<extra_domain(s)> ...]`  
Here, `<target>` is the server to proxy (e.g. `http://localhost:8000`) which _must_ include the protocol.
It is expected that this will be a private server on the local network, rather than a file path or the public domain of another site.  
`<main_domain>` is the public domain name for the new site (e.g. `www.example.com`) which _must not_ include the protocol.
Additional public domain names can also be provided if required.
These are not treated differently to `<main_domain>`, other than the latter is used to name the config file and certificates.

The configuration files created by `add_site.sh` are not explicitly managed by Gatekeeper going forwards.
As such, it perfectly fine to rename the files or alter their contents - Guides for NGINX configuration are available online.
After making any changes, the NGINX service should be restarted by calling `docker-compose up -d --force-recreate nginx`.
If manually creating/renaming config files, note that files matching `config/*.site[s].conf[.disabled]` will be ignored by Gatekeeper's git.


## Certificate renewal
Gatekeeper provides a script, `renew_certs.sh`, for certificate renewal and redeployment.
It triggers certbot to renew all active certificates and then reloads NGINX to deploy them.
It can be executed manually simply by calling `./renew_certs.sh`.
__The script must be called from the Gatekeeper root.__

### Automatic renewals
It is recommended to run the above script on a schedule, to ensure certificates are always up-to-date without manual intervention.
Gatekeeper relies on the scheduling functionality of the host operating system to provide this functionality.  

Call `crontab -e` to edit the scheduler and add a job to run `cd /path/to/gatekeeper && ./renew_certs.sh` - Do not concatinate the commands.
Information on the format for crontab files can be readily found online, e.g. [here](https://help.ubuntu.com/community/CronHowto).
It is not necessary to `sudo` the `crontab -e` command, if the current user has docker permissions.

Let's Encrypt/Certbot recommend 12 hours between renewal jobs, although a daily overnight task is also acceptable.


## Extensions

### Default response
If NGINX can't find a matching server for a request, by default it will pick a server to send the requests on to.
This may produce undesirable behaviour upsteam (security holes, error reports, etc.).
To solve this problem, Gatekeeper provides two configuration templates `config/default-404.conf.template` and `config/default-444.conf.template`.
These give `404 Not Found` and `444 Connection Closed` responses respectively.

To activate one of these, copy the template file and replace `<domain>` with the equivalent value from a site-specific config files.
While the certificate will not match the host for extraneous requests handled by this default server, NGINX nevertheless requires a certificate to be provided. 
Repurposing an existing certificate is simpler than generating self-signed certificates or sourcing general certificates.
Note that files matching `config/default*.conf[.disabled]` will be ignored by Gatekeeper's git.
