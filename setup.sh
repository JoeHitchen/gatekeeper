#!/bin/bash
docker-compose run --rm --entrypoint /usr/bin/openssl certbot dhparam -out /etc/letsencrypt/dhparam.pem 2048
docker-compose up -d --force-recreate nginx

echo $1 > settings/admin_email.txt
